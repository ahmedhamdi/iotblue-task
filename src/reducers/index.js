import { combineReducers } from 'redux';
import * as reducres from './reducer';
import { createStore } from 'redux'
export const rootReducer = combineReducers({
  ...reducres,
})
const store = createStore(rootReducer)
export default store