import {get} from "lodash";
export const add_item =(state={},payload)=>{
    return {
        ...state,
        item:[...get(state,"item",[]), payload.item]
    }
}