import * as handlers from './handler';

export const cart =(state={},action)=>{
    return handlers[action.type]
    ? handlers[action.type](state,action)
    :state;
}