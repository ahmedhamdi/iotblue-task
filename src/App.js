import React, { Component } from 'react';
import './App.css';
import ItemComponent from "./Containers/ItemPage";
import CartComponent from "./Containers/CartComponent";
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
        <section>
          <Switch>
            <Route exact path="/" component={ItemComponent} />
            <Route path="/cart" component={CartComponent}/>
            </Switch>             
                </section>
       </Router>
    );
  }
}

export default App;
