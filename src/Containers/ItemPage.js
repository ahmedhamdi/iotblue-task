import React, { Component } from 'react';
import ImgMediaCard from "../Component/Card";
import {Item_data} from "./Item_data";
import IconButton from '@material-ui/core/IconButton';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import { connect } from 'react-redux';
import {NavLink,withRouter} from 'react-router-dom';
import {mapDispatchToProps,mapstatetoprops} from "./mapping";
import "./style.css"

class ItemComponent extends Component {

   adding (item){
     console.log("yes")
     this.props.addItem(item)
   }
    render() {
      return (
        <div>

        <NavLink to="/cart">
       {this.props.item&& <IconButton color="primary" aria-label="Add to shopping cart">
           <AddShoppingCartIcon />
      </IconButton>  }
         </NavLink>

         <div className="App">
           {Item_data.map((data,index)=>(
            <ImgMediaCard name={data.name} image={data.image} disc={data.discription} add_item_fun={this.adding.bind(this,data)}/>
        ))} 
        </div>

        </div>
      );
    }
  }
  
export default connect(mapstatetoprops,mapDispatchToProps)(withRouter(ItemComponent));
