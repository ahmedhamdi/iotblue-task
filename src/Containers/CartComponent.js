import React, { Component } from 'react';
import {mapstatetoprops} from "./mapping";
import { connect } from 'react-redux';
import {map} from "lodash";
import "./style.css"

class CartComponent extends Component{
    render(){
        return(
            <div >
                <div>{this.props.item.length} item in your cart</div>
            {map(this.props.item,(v,i)=>(
                  <section className="continer"> 
                  <img src={v.image}/>
                  <div>{v.name}</div>
                  <div>{v.discription}</div>
              </section>
            ))}
            </div>
        );
       
    }
}
export default connect(mapstatetoprops,null)(CartComponent);